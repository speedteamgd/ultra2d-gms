#define u2d_menu_debug
/// u2d_menu_debug();
__menuanim_menu_draw = u2d_menu_debug_draw;
__menuanim_item_draw = u2d_menu_debug_item;
__menuanim_yesno_draw = u2d_menu_debug_yn;
__menuanim_range_draw = u2d_menu_debug_range;
__menuanim_switch_draw = u2d_menu_debug_switch;

#define u2d_menu_debug_draw
/// Debug Menu Skin
draw_set_alpha(0.7);
draw_rectangle_colour(view_xview,view_yview,view_xview+200,view_yview+view_hview,c_black,c_black,c_black,c_black,false);
draw_text_ext_colour(view_xview+3,view_yview+3,
"[ Debug info ]"+
"#Menu current: "+string(__menu_current)+
"#Menu next: "+string(__menu_new)+
"#Select current: "+string(__menu_item)+
"#Select next: "+string(__menu_item_new)+
"#Select last: "+string(__menu_item_max)+
"#[ Changing ]"+
"#Menu change: "+string(__menu_change)+
"#Select change: "+string(__menu_item_change)+
"#Value change: "+string(__menu_value_change)+
"#[ Settings ]"+
"#Select looping: "+string(__menu_prop_looping)+
"#Only Selected Val: "+string(__menu_prop_show_selval)
,-1,190,c_white,c_white,c_white,c_white,1);
draw_set_alpha(1);

draw_text(200,5,"-[ "+string(argument0)+" ]-");

#define u2d_menu_debug_item
/// Debug Menu Skin
var xx = 200;
var yy = 30+20*argument0;
var text = string(argument1);
draw_text(xx+20,yy,text);
if(argument2) draw_text(xx,yy,"->");

u2d_menu_sensor(xx,yy,xx+string_width(text)+20,yy+string_height(text),"check",argument0);
//draw_rectangle(xx,yy,xx+string_width(text)+20,yy+string_height(text),true);

#define u2d_menu_debug_yn
var text;
if(argument1)text="Yes"; else text="No";
draw_text(380,30+20*argument0,text);

#define u2d_menu_debug_range
var text = string_insert('|','....',round(5*argument1/10))
draw_text(380,30+20*argument0,"[ "+text+" ] "+string(argument1));

#define u2d_menu_debug_switch
// i,string_array,item[?'value']
var text;
draw_text(380,30+20*argument0,argument1[argument2]);
