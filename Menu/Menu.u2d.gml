#define u2d_menu_init
/// u2d_menu_init()
// ����
__menu_list =    ds_list_create();      // ������ ���� ���� (��������� ��� �����������)
__menu_current            = noone;      // ������� ����
__menu_new                = noone;      // ����� ����

// ������ ����
__menu_items              = noone;      // ������ �������
__menu_item               = 0;          // �������� �����
__menu_item_new           = 0;          // ����� �����
__menu_item_max           = 0;          // ��������� �����

// ����� ���������
__menu_change             = false;      // ����� ����
__menu_item_change        = false;      // ����� ������ ����
__menu_value_change       = false;      // ����� ��������
__menu_animated           = true;

// ���������
__menu_prop_looping       = false;      // ����������� �����
__menu_prop_show_selval   = false;      // ���������� �������� ������ ���������� ������

// ����������
__menukey_select_up       = vk_up;      // ������� ������ ������ ���� -1
__menukey_select_down     = vk_down;    // ������� ������ ������ ���� +1
__menukey_value_prev      = vk_left;    // ������� ������ �������� -1
__menukey_value_next      = vk_right;   // ������� ������ �������� +1
__menukey_select          = vk_enter;   // ������� �����
__menukey_back            = vk_escape;  // ������� ��������

__menulock_select         = false;      // ����������� ��������� ������
__menulock_value          = false;      // ����������� ��������� ��������

__menu_select_up          = false;
__menu_select_down        = false;
__menu_value_next         = false;
__menu_value_prev         = false;
__menu_select             = false;
__menu_back               = false;

// ������
__menu_sensor_active      = false;
__menu_sensor_type        = "";
__menu_sensor_ind         = noone;
__menu_sensor_x           = 0;
__menu_sensor_y           = 0;
__menu_sensor_slide_dir   = 0;
__menu_sensor_slide_dis   = 0;
__menu_sensor_slide_xdis  = 0;
__menu_sensor_slide_ydis  = 0;

// �������� ���������

__menuanim_menu_step = noone;
__menuanim_menu_change_step = noone;
__menuanim_menu_draw = noone;

__menuanim_item_step = noone;
__menuanim_item_change_step = noone;
__menuanim_item_draw = noone;

__menuanim_yesno_next_step = noone;
__menuanim_yesno_prev_step = noone;
__menuanim_yesno_draw = noone;

__menuanim_range_next_step = noone;
__menuanim_range_prev_step = noone;
__menuanim_range_draw = noone;

__menuanim_switch_next_step = noone;
__menuanim_switch_prev_step = noone;
__menuanim_switch_draw = noone;

#define u2d_menu_update
/// u2d_menu_update()
if(!ds_exists(__menu_list,ds_type_list))return 0;

// ������� �� ������
__menu_select_up = keyboard_check_pressed(__menukey_select_up);
__menu_select_down = keyboard_check_pressed(__menukey_select_down);
__menu_value_next = keyboard_check_pressed(__menukey_value_next);
__menu_value_prev = keyboard_check_pressed(__menukey_value_prev);
__menu_select = keyboard_check_pressed(__menukey_select);
__menu_back = keyboard_check_pressed(__menukey_back);

// ��������� �������
if(__menu_sensor_active){
  switch(__menu_sensor_type){
    case "sel-": __menu_select_down = true; break;
    case "sel+": __menu_select_up = true; break;
    case "val-": __menu_value_prev = true; break;
    case "val+": __menu_value_next = true; break;
    case "check":
         if(__menu_sensor_ind!=noone){
            if(__menu_sensor_ind != __menu_item){
               __menu_item_new=__menu_sensor_ind;
               __menu_item_change=true;
            } else __menu_select = true;
         }
    break;
  }
  
  if(__menu_sensor_slide_ydis > 100){
     if(__menu_sensor_slide_dir < 180) __menu_select_up=true;
     if(__menu_sensor_slide_dir > 180) __menu_select_down=true;
  }
  
  __menu_sensor_active = false;
  __menu_sensor_type = "";
  __menu_sensor_ind = noone;
}

if(script_exists(__menuanim_menu_step))script_execute(__menuanim_menu_step);

// �������� ���������� ����
/*
if(__menu_change){__menu_change=false; __menu_current=__menu_new; u2d_menu_current(__menu_new,false);}
if(__menu_item_change){__menu_item_change=false; __menu_item=__menu_item_new;}
if(__menu_value_change){__menu_value_change=false;}
*/
// ���������� ����
if(__menu_value_change){__menu_value_change=false;}
if(__menu_item_change){if(script_exists(__menuanim_item_change_step)){script_execute(__menuanim_item_change_step); __menu_item_change=!__menu_animated;} else {u2d_menu_apply_changes(); __menu_item_change=false;}}
if(__menu_change){if(script_exists(__menuanim_menu_change_step)){script_execute(__menuanim_menu_change_step); __menu_change=!__menu_animated;} else {u2d_menu_apply_changes(); __menu_change=false;}}

// ��������� ����
if(!__menu_change && !__menu_item_change && !__menu_value_change){
   if not __menulock_select {
       if(__menu_select_up){
           if(__menu_item==0){if(__menu_prop_looping){__menu_item_new=__menu_item_max; __menu_item_change=true; __menu_animated=false;}}
           else {__menu_item_new=__menu_item-1; __menu_item_change=true; __menu_animated=false;}
       }
       if(__menu_select_down){
           if(__menu_item==__menu_item_max){if(__menu_prop_looping){__menu_item_new=0; __menu_item_change=true; __menu_animated=false;}}
           else {__menu_item_new=__menu_item+1; __menu_item_change=true; __menu_animated=false;}
       }
   }
   
   if not __menulock_value && (__menu_value_next || __menu_value_prev) {
       var __menu_item_temp = __menu_items[|__menu_item];
       switch(__menu_item_temp[?"type"]){
          case 3: __menu_item_temp[?'value'] =! __menu_item_temp[?'value']; break;
          case 4:
            var imin,imax;
            if ds_map_exists(__menu_item_temp,'min') imin = __menu_item_temp[?"min"]; else imin = 0;
            if ds_map_exists(__menu_item_temp,'max') imax = __menu_item_temp[?"max"]; else imax = -1;
            if(__menu_value_prev){ if(__menu_item_temp[?"value"]>imin)__menu_item_temp[?"value"]--; }
            if(__menu_value_next){ if(imax!=-1){if(__menu_item_temp[?"value"]<imax)__menu_item_temp[?"value"]++;}else __menu_item_temp[?"value"]++; }
          break;
          case 5:
            var s=0;
            while(ds_map_exists(__menu_item_temp,'s'+string(s)))s++;s--;
            if(__menu_value_prev){ if(__menu_item_temp[?"value"]>0)__menu_item_temp[?"value"]--; else {if(__menu_item_temp[?"loop"]==true)__menu_item_temp[?"value"]=s;} }
            if(__menu_value_next){ if(__menu_item_temp[?"value"]<s)__menu_item_temp[?"value"]++; else {if(__menu_item_temp[?"loop"]==true)__menu_item_temp[?"value"]=0;} }
          break;
       }
   }
   
   if(__menu_select){
      var __menu_item_temp = __menu_items[|__menu_item];
      switch(__menu_item_temp[?"type"]){
         case 1: if(ds_map_exists(__menu_item_temp,'menu'))u2d_menu_current(__menu_item_temp[?'menu'],true); break;
         case 2: if(ds_map_exists(__menu_current,'parrent'))u2d_menu_current(__menu_current[?'parrent'],true); break;
         case 3: __menu_item_temp[?'value']=!__menu_item_temp[?'value']; break;
         case 6: if(ds_map_exists(__menu_item_temp,'scr')){ if(script_exists(__menu_item_temp[?"scr"]))script_execute(__menu_item_temp[?"scr"],__menu_item,__menu_item_temp[?"name"],__menu_current[?"name"]); } break;
         case 7: if(ds_map_exists(__menu_item_temp,"user"))event_user(__menu_item_temp[?"user"]); break;
      }
   }
}
if(script_exists(__menuanim_item_step))script_execute(__menuanim_item_step,__menu_item,__menu_item_max);

#define u2d_menu_current
/// u2d_menu_current( menu, transition );
if(argument1 == true){
  __menu_new = argument0;
  __menu_change = true;
  __menu_animated = false;
} else {
  __menu_current = argument0;
  __menu_item = 0;
  __menu_items = argument0[?'items'];
  __menu_item_max = ds_list_size(__menu_items)-1;
}

#define u2d_menu_new
/// u2d_menu_new( name, parrent );
if(argument_count>=1 && is_string(argument[0])){
    var menu_id = ds_map_create(); ds_list_add(__menu_list,menu_id);
    ds_map_add(menu_id,'name',argument[0]);
    ds_map_add(menu_id,'items',ds_list_create());
    if(argument_count==2 && ds_exists(argument[1],ds_type_map))
     ds_map_add(menu_id,'parrent',argument[1]);
    return menu_id;
} return noone;

#define u2d_menu_item_new
/// u2d_menu_item_new( name, type )
// ------------------------------------------- >>
// arg0 [name] - �������� ������ ����
// arg1 [type] - ��� ������
//    |- 0 : none - ������ �� ������
//    |- 1 : goto - ������� � ����
//       |- menu - id ����
//    |- 2 : back - ������� � ���������� ����
//    |- 3 : yesno - ������������� Yes / No
//       |- yesno - �� ��������� true/false
//    |- 4 : range - �������
//       |- min - ����������� ��������
//       |- max   - ������������ ��������
//    |- 5 : switch - ������������� �����
//       |- s1,s2..sx - ��������� ������
//       |- loop - ��������� �����
//    |- 6 : call - ������� ������
//         | - src - ������
//    |- 7 : user
//         | - user - ����� �������
// ------------------------------------------- >>

if(is_string(argument0) && is_real(argument1)){
    var menu_item_id = ds_map_create();
    ds_map_add(menu_item_id,'name',argument0);
    ds_map_add(menu_item_id,'type',argument1);
    switch(argument1){
       case 3: ds_map_add(menu_item_id,'yesno',false); break;
       case 5: ds_map_add(menu_item_id,'loop',false);
       case 4: ds_map_add(menu_item_id,'value',false); break;
    }
    return menu_item_id;
} else return noone;

#define u2d_menu_add_item
/// u2d_menu_add_item( menu, item1, ...)
if argument_count > 1{
  var menu_items = argument[0]; menu_items=menu_items[?"items"];
  if argument_count > 2{
      for(var i=1; i<argument_count;i++){
        if(ds_exists(argument[i],ds_type_map))
        ds_list_add(menu_items,argument[i]);
      }
  } else if(ds_exists(argument[1],ds_type_map)) ds_list_add(menu_items,argument[1]);
  return true;
} return false;

#define u2d_menu_item_prop
/// u2d_menu_item_prop( item, prop, value );
ds_map_replace( argument0, argument1, argument2 );

#define u2d_menu_draw
/// u2d_menu_draw()
if(!ds_exists(__menu_list,ds_type_list))return 0;
if(script_exists(__menuanim_menu_draw))script_execute(__menuanim_menu_draw,__menu_current[?"name"]);
if(script_exists(__menuanim_item_draw)){
  var items = __menu_current[?"items"];
  for(var i=0;i<ds_list_size(items);i++){
    var item = ds_list_find_value(items,i);
    var is_selected = (i==__menu_item);
    script_execute(__menuanim_item_draw,i,item[?"name"],is_selected);
    if(__menu_prop_show_selval && !is_selected)continue;
    switch(item[?"type"]){
      case 3: if script_exists(__menuanim_yesno_draw)script_execute(__menuanim_yesno_draw,i,item[?"value"]); break;
      case 4: 
        if script_exists(__menuanim_range_draw){
        var ival,imin,imax;
        if ds_map_exists(item,'value') ival = item[?"value"]; else ival = 0;
        if ds_map_exists(item,'min') imin = item[?"min"]; else imin = -1;
        if ds_map_exists(item,'max') imax = item[?"max"]; else imax = -1;
        script_execute(__menuanim_range_draw,i,ival,imin,imax);
        }
      break;
      case 5:
        if script_exists(__menuanim_switch_draw){
        var string_array,s=0;
        while(ds_map_exists(item,'s'+string(s))){string_array[s]=item[?'s'+string(s)]; s++;}
        if s!=0 script_execute(__menuanim_switch_draw,i,string_array,item[?'value']);
        }
      break;
    }
  }
}

#define u2d_menu_destroy
/// u2d_menu_destroy() // Destroy Menu's Data...
if(ds_exists(__menu_list,ds_type_list)){
    for(var i=0;i<ds_list_size(__menu_list);i++){
       var __menu_id = __menu_list[|i];
       if(ds_exists(__menu_id,ds_type_map)){
           var __menu_items = __menu_id[?"items"];
           if(ds_exists(__menu_items,ds_type_list)){
               if(ds_list_size(__menu_items) > 0)
               for(var j=0;j<ds_list_size(__menu_items);j++)
               ds_map_destroy(__menu_items[|j]);
           ds_list_destroy(__menu_items);
           }
       ds_map_destroy(__menu_id);
       }
    }
    ds_list_destroy(__menu_list);
}
#define u2d_menu_sensor
///u2d_menu_sensor(x1,y1,x2,y2,type,ind)
if !__menu_sensor_active{
var x_start = min(argument0,argument2);
var x_end = max(argument0,argument2);
var y_start = min(argument1,argument3);
var y_end = max(argument1,argument3);

var xs = device_mouse_x(0);
var ys = device_mouse_y(0);

var active = (xs>=x_start && xs<=x_end) && (ys>=y_start && ys<=y_end);
    if(active && device_mouse_check_button_pressed(0,mb_left)){
       __menu_sensor_active = active;
       __menu_sensor_type = string(argument4);
       __menu_sensor_ind = argument5;
       __menu_sensor_x = xs;
       __menu_sensor_y = ys;
       __menu_sensor_slide_xdis = 0;
       __menu_sensor_slide_ydis = 0;
    }
}

#define u2d_menu_apply_changes
if(__menu_change){u2d_menu_current(__menu_new,false);}
if(__menu_item_change){__menu_item=__menu_item_new;}
