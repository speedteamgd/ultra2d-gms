#define u2d_anim_init
/// u2d_anim_init();

// Name
anim_name = "";
anim_name_prev = string(noone);
anim_name_next = "";

// Group
anim_group = "";
anim_group_prev = string(noone);

// Key Frames
anim_frame = 0;
anim_frame_start = 0;
anim_frame_end = 0;
anim_frame_loop = 0

// Options
anim_sprite = sprite_index;
anum_repeat = 0;
anim_alpha = 1; image_alpha = 1;
anim_angle = 0; image_angle = 0;
anim_xscale = 1; image_xscale = 1;
anim_yscale = 1; image_yscale = 1;

// Sprite Part
anim_part = false;
anim_part_x = 0; anim_part_xto = -1; anim_part_speed_x = 0;
anim_part_y = 0; anim_part_yto = -1; anim_part_speed_y = 0;
anim_part_w = 0; anim_part_wto = -1; anim_part_speed_w = 0;
anim_part_h = 0; anim_part_hto = -1; anim_part_speed_h = 0;

// Smooth Transition
anim_smooth_alpha = false;
anim_smooth_xscale = false;
anim_smooth_yscale = false;
anim_smooth_angle = false;
anim_smooth_part  = false;

// Speed
anim_speed=0; image_speed=0;

// Anim System
anim_rollback = false;
anim_isnew = true;
anim_oneframe = true;
anim_done = false;
anim_memory = ds_map_create();

#define u2d_anim_update
/// u2d_anim_update();
anim_isnew = anim_group_prev!=anim_group || anim_name_prev!=anim_name;

// Update animation data
if anim_isnew {
   // Set new animation
   anim_group_prev=anim_group;
   anim_name_prev=anim_name;
   
   // Get path to animation data
   var mask = string(anim_group)+'.'+string(anim_name_prev)+'.';
   
   // Load animation data
   anim_name_next          = ds_map_find_value(anim_memory,mask+"next");
   anim_sprite             = ds_map_find_value(anim_memory,mask+"sprite");
   anim_frame_start        = ds_map_find_value(anim_memory,mask+"start");
   anim_frame_end          = ds_map_find_value(anim_memory,mask+"end");
   anim_frame_loop         = ds_map_find_value(anim_memory,mask+"loop");
   anim_repeat             = ds_map_find_value(anim_memory,mask+"repeat");
   anim_alpha              = ds_map_find_value(anim_memory,mask+"alpha");
   anim_angle              = ds_map_find_value(anim_memory,mask+"angle");
   anim_xscale             = ds_map_find_value(anim_memory,mask+"xscale");
   anim_yscale             = ds_map_find_value(anim_memory,mask+"yscale");
   anim_speed              = ds_map_find_value(anim_memory,mask+"speed");
   anim_part               = ds_map_find_value(anim_memory,mask+"part");
   if(anim_part){
     anim_part_x = anim_part_xto;
     anim_part_xto         = ds_map_find_value(anim_memory,mask+"part.x");
     anim_part_y = anim_part_yto;
     anim_part_yto         = ds_map_find_value(anim_memory,mask+"part.y");
     anim_part_w = anim_part_wto;
     anim_part_wto         = ds_map_find_value(anim_memory,mask+"part.w");
     anim_part_h = anim_part_hto;
     anim_part_hto         = ds_map_find_value(anim_memory,mask+"part.h");
     
     if(anim_part_x<0)anim_part_x = anim_part_xto;
     if(anim_part_y<0)anim_part_y = anim_part_yto;
     if(anim_part_w<0)anim_part_w = anim_part_wto;
     if(anim_part_h<0)anim_part_h = anim_part_hto;
   }
   
   // Transitions
   anim_smooth_alpha       = ds_map_find_value(anim_memory,mask+"smooth.alpha");
   anim_smooth_xscale      = ds_map_find_value(anim_memory,mask+"smooth.xscale");
   anim_smooth_yscale      = ds_map_find_value(anim_memory,mask+"smooth.yscale");
   anim_smooth_angle       = ds_map_find_value(anim_memory,mask+"smooth.angle");
   anim_smooth_part        = ds_map_find_value(anim_memory,mask+"smooth.part");
   
   
   // Setup system
   sprite_index = anim_sprite;
   anim_rollback = anim_speed<0;
   if not abs(anim_speed)<1 anim_speed=anim_speed/100;
   if(anim_frame_start > anim_frame_end){
     var frame = anim_frame_start;
     anim_frame_start = anim_frame_end;
     anim_frame_end = frame;
   }
   var frame_count = anim_frame_end-anim_frame_start;
   anim_oneframe = frame_count == 0;
   if(anim_rollback) anim_frame = anim_frame_end; else anim_frame = anim_frame_start;
   if(anim_frame_loop < anim_frame_start || anim_frame_loop > anim_frame_end) anim_frame_loop = anim_frame;
   
   // Transitions
   if(anim_speed!=0){
       if(anim_smooth_alpha && !anim_oneframe) anim_smooth_alpha = (anim_alpha - image_alpha)*abs(anim_speed) / frame_count;
           else {image_alpha = anim_alpha; anim_smooth_alpha = 0;}
       if(anim_smooth_angle && !anim_oneframe) anim_smooth_angle = (anim_angle - image_angle)*abs(anim_speed) / frame_count;
           else {image_angle = anim_angle; anim_smooth_angle = 0;}
       if(anim_smooth_xscale && !anim_oneframe) anim_smooth_xscale = (abs(anim_xscale) - abs(image_xscale))*abs(anim_speed) / frame_count;
           else {image_xscale = anim_xscale; anim_smooth_xscale = 0;}
       if(anim_smooth_yscale && !anim_oneframe) anim_smooth_yscale = (abs(anim_yscale) - abs(image_yscale))*abs(anim_speed) / frame_count;
           else {image_yscale = anim_yscale; anim_smooth_yscale = 0;}
           
       if(anim_part && anim_smooth_part && !anim_oneframe){
         anim_part_speed_x = (anim_part_xto-anim_part_x)*abs(anim_speed) / frame_count;
         anim_part_speed_y = (anim_part_yto-anim_part_y)*abs(anim_speed) / frame_count;
         anim_part_speed_w = (anim_part_wto-anim_part_w)*abs(anim_speed) / frame_count;
         anim_part_speed_h = (anim_part_hto-anim_part_h)*abs(anim_speed) / frame_count;
       } else {
         anim_smooth_part = false;
         anim_part_x = anim_part_xto;
         anim_part_y = anim_part_yto;
         anim_part_w = anim_part_wto;
         anim_part_h = anim_part_hto;
       }
   }
}

// Animation System
if not anim_done && anim_speed!=0 {
  anim_frame += anim_speed;
  
  if(anim_smooth_alpha!=0){image_alpha+=anim_smooth_alpha; if(image_alpha == anim_alpha)anim_smooth_alpha=0;}
  if(anim_smooth_angle!=0){image_angle+=anim_smooth_angle; if(round(image_angle) == anim_angle)anim_smooth_angle=0;}
  if(anim_smooth_xscale!=0){image_xscale+=anim_smooth_xscale; if(image_xscale == anim_xscale)anim_smooth_xscale=0;}
  if(anim_smooth_yscale!=0){image_yscale+=anim_smooth_yscale; if(image_yscale == anim_yscale)anim_smooth_yscale=0;}
  
  if(anim_part && anim_smooth_part){
    if(anim_part_speed_x!=0){anim_part_x+=anim_part_speed_x; if(anim_part_x == anim_part_xto)anim_part_speed_x=0; }
    if(anim_part_speed_y!=0){anim_part_y+=anim_part_speed_y; if(anim_part_y == anim_part_yto)anim_part_speed_y=0; }
    if(anim_part_speed_w!=0){anim_part_w+=anim_part_speed_w; if(anim_part_w == anim_part_wto)anim_part_speed_w=0; }
    if(anim_part_speed_h!=0){anim_part_h+=anim_part_speed_h; if(anim_part_h == anim_part_hto)anim_part_speed_h=0; }
  }
  
  if (!anim_rollback && anim_frame>anim_frame_end) || (anim_rollback && anim_frame<anim_frame_start){
    if(anim_repeat == 0){
      if(anim_name_next == string(noone)){
        anim_done = true;
        if(anim_rollback)anim_frame = anim_frame_end;
        else anim_frame = anim_frame_start;
      } else anim_name = anim_name_next;
    } else {
      if(anim_repeat>0)anim_repeat--;
      if(anim_rollback)anim_frame = anim_frame_end;
      else anim_frame = anim_frame_start;
    }
  }
  image_index = round(anim_frame);
}

#define u2d_anim_draw
/// u2d_anim_draw(x,y);
if(anim_part){
    draw_sprite_general(sprite_index,image_index,
    anim_part_x,anim_part_y,anim_part_w,anim_part_h,
    argument0,argument1,
    image_xscale,image_yscale,image_angle,
    image_blend,image_blend,image_blend,image_blend,image_alpha);}
else draw_sprite_ext(sprite_index,image_index,argument0,argument1,image_xscale,image_yscale,image_angle,image_blend,image_alpha);

#define u2d_anim_create
/// u2d_anim_create(group, name, start, end, speed);
var mask = string(argument0)+'.'+string(argument1)+'.';
ds_map_replace(anim_memory,mask+"next",string(noone));
ds_map_replace(anim_memory,mask+"sprite",sprite_index);
ds_map_replace(anim_memory,mask+"start",argument2);
ds_map_replace(anim_memory,mask+"end",argument3);
ds_map_replace(anim_memory,mask+"loop",argument2);
ds_map_replace(anim_memory,mask+"repeat",-1);
ds_map_replace(anim_memory,mask+"alpha",1);
ds_map_replace(anim_memory,mask+"angle",0);
ds_map_replace(anim_memory,mask+"xscale",1);
ds_map_replace(anim_memory,mask+"yscale",1);
ds_map_replace(anim_memory,mask+"speed",argument4);
ds_map_replace(anim_memory,mask+"part",false);
ds_map_replace(anim_memory,mask+"part.x",0);
ds_map_replace(anim_memory,mask+"part.y",0);
ds_map_replace(anim_memory,mask+"part.w",0);
ds_map_replace(anim_memory,mask+"part.h",0);
ds_map_replace(anim_memory,mask+"smooth.alpha",false);
ds_map_replace(anim_memory,mask+"smooth.xscale",false);
ds_map_replace(anim_memory,mask+"smooth.yscale",false);
ds_map_replace(anim_memory,mask+"smooth.angle",false);
ds_map_replace(anim_memory,mask+"smooth.part",false);

#define u2d_anim_current_group
/// u2d_anim_current_group( [group] )
if(argument_count == 1)anim_group=string(argument[0]);
return anim_group;

#define u2d_anim_current_anim
/// u2d_anim_current_anim( [name] )
if(argument_count == 1)anim_name=string(argument[0]);
return anim_name;

#define u2d_anim_set_basic
/// u2d_anim_set_basic(group, name, start, end, speed);
var mask = string(argument0)+'.'+string(argument1)+'.';
ds_map_replace(anim_memory,mask+"start",argument2);
ds_map_replace(anim_memory,mask+"end",argument3);
ds_map_replace(anim_memory,mask+"loop",argument2);
ds_map_replace(anim_memory,mask+"speed",argument4);

#define u2d_anim_set_part
/// u2d_anim_set_part(group, name, x, y, w, h, smooth);
var mask = string(argument0)+'.'+string(argument1)+'.';
if(argument2<0)argument2=0;
if(argument3<0)argument3=0;
if(argument4<1 || argument5<1)return 0;
ds_map_replace(anim_memory,mask+"part",true);
ds_map_replace(anim_memory,mask+"part.x",argument2);
ds_map_replace(anim_memory,mask+"part.y",argument3);
ds_map_replace(anim_memory,mask+"part.w",argument4);
ds_map_replace(anim_memory,mask+"part.h",argument5);
ds_map_replace(anim_memory,mask+"smooth.part",argument6);

#define u2d_anim_set_loop
/// u2d_anim_set_loop(group, name, loop frame, repeat);
var mask = string(argument0)+'.'+string(argument1)+'.';
ds_map_replace(anim_memory,mask+"loop",argument2);
ds_map_replace(anim_memory,mask+"repeat",argument3);

#define u2d_anim_set_sprite
/// u2d_anim_set_sprite(group, name, sprite)
ds_map_replace(anim_memory,string(argument0)+'.'+string(argument1)+".sprite",argument2);

#define u2d_anim_set_transform
/// u2d_anim_set_transform(group, name, angle, xscale, yscale, alpha);
var mask = string(argument0)+'.'+string(argument1)+'.';
ds_map_replace(anim_memory,mask+"angle",argument2);
ds_map_replace(anim_memory,mask+"xscale",argument3);
ds_map_replace(anim_memory,mask+"yscale",argument4);
ds_map_replace(anim_memory,mask+"alpha",argument5);

#define u2d_anim_set_smooth
/// u2d_anim_set_smooth(group, name, angle, xscale, yscale, alpha);
var mask = string(argument0)+'.'+string(argument1)+'.';
ds_map_replace(anim_memory,mask+"smooth.angle",argument2);
ds_map_replace(anim_memory,mask+"smooth.xscale",argument3);
ds_map_replace(anim_memory,mask+"smooth.yscale",argument4);
ds_map_replace(anim_memory,mask+"smooth.alpha",argument5);

#define u2d_anim_set_next
/// u2d_anim_set_next(group, name, next)
ds_map_replace(anim_memory,string(argument0)+'.'+string(argument1)+".next",string(argument2));

#define u2d_anim_set_var
/// u2d_anim_set_var(group, name, var, value)
ds_map_replace(anim_memory,string(argument0)+'.'+string(argument1)+'.'+string(argument2),string(argument3));

#define u2d_anim_get_var
/// u2d_anim_get_var(group, name, var);
ds_map_find_value(anim_memory,string(argument0)+'.'+string(argument1)+'.'+string(argument2));
