#define u2d_cmd_init
///u2d_cmd_init()
cmd_commands = ds_map_create();
cmd_data = ds_map_create();
cmd_vars = ds_map_create();
cmd_savedata = false;
cmd_return = '';
cmd_print = noone;

#define u2d_cmd_handler
//u2d_cmd_handler(command)


// ------------------------------
// -> Init

cmd_return = '';

// Command
var cmd_str = string(argument0)+' ';
var cmd_len = string_length(cmd_str);
var cmd_args = 0;

// Reader
var rd_pos = 0;
var rd_char = '';
var rd_lchar = '';
var rd_word = '';
var rd_key = '';
var rd_buffer = '';
var rd_on = true;

// Flags
var is_name = true;
var is_str = false; // Строка
var is_rvar = false; // читать переменную
var is_wvar = false; // записать переменную
var is_val = false; // Значение
var is_flag = 0; // Флаг
var is_subcmd = false; // Подкоманда
var is_done = false; // Завершить
var is_busy = false; // Занят
var is_not_control = false; // не управляемый символ
var is_end = false; // Выход из обработки

// Command
var sub_cmd_lvl = 0;
var return_to_var = '';
var statement = '';
var wait_for_value = false;
var skip_cmd_at = -1;
var looping = false;

if not cmd_savedata cmd_data = ds_map_create();

// ------------------------------
// -> reading data

while(rd_pos<cmd_len){
  rd_lchar = rd_char;
  rd_char = string_char_at(cmd_str,++rd_pos); 
  rd_on=true;
  is_busy = is_str || is_subcmd;
  
  if(is_not_control){is_not_control=false; rd_word+=rd_char; continue;}
  else if(rd_char == '/'){is_not_control=true; rd_on=false;}
  
  // IF & Loop
  if((rd_word=="if" || rd_word='loop') && !is_busy){
     wait_for_value = true;
     if(is_wvar && is_val && rd_word='loop'){return_to_var=rd_key; is_wvar=false; is_val=false;}
     statement = rd_word;
     rd_word='';
  }
  
  // String - Read
  if(rd_char == '"' && !is_subcmd){
     if(is_str){
         if(rd_buffer!=''){rd_word=rd_buffer+rd_word; rd_buffer='';}
         if(!is_val){is_val=true; rd_key='arg'+string(cmd_args++);}
         is_done=true; rd_pos++; 
     }
     is_str=!is_str;
     rd_on=false;
  }
  
  // Flag
  if(rd_char=='-' && !is_val && !is_busy){
    if(is_flag<2)is_flag++;
    rd_on=false;
  }
  
  // Variable - Write
  if(rd_char == '@' && !is_busy){
     is_wvar=true; rd_on=false;
  }
  if(is_wvar && !is_val && rd_char ==':')rd_on=false;
  
  if((rd_char==' ' || rd_char=='' || rd_char=='#') && !is_busy){
      if(is_wvar && is_val && !is_subcmd) || (string_length(rd_word)>0) is_done=true;
      if(rd_char=='#')is_end=true;
      rd_on=false; 
  }
  
  // Variable - Read
  if(rd_char == '('  && !is_subcmd){
     is_rvar=true; rd_on=false;
     if(is_str){rd_buffer=rd_word; rd_word='';}
  }
  if(is_rvar && rd_char==')'){
     if(is_str){rd_buffer+=string(cmd_vars[?rd_word]); rd_word=rd_buffer; rd_buffer=''; is_rvar=false;}else{is_done=true; rd_pos++;}
     rd_on=false;
     }
  
  if(rd_char == "{" && !is_str){
     if(!is_subcmd){rd_on=false;}
     is_subcmd = true; sub_cmd_lvl++;
  }
  if(is_subcmd && rd_char=='}'){
     show_debug_message("subcmd: "+rd_word+"; lvl="+string(sub_cmd_lvl));
     sub_cmd_lvl--; if(sub_cmd_lvl==0){is_done=true; rd_pos++; rd_on=false;}
  }
  
  if(rd_char=='=' && !is_busy){
     is_val = true; rd_on=false;
     if is_wvar{rd_key = rd_word; if(is_name && rd_lchar == ":"){
        return_to_var=rd_word; is_val=false; is_wvar=false;
        } rd_word='';}
     else {
        if(is_name){rd_key = rd_word; is_done=true;}
        else {if(rd_word!=''){rd_key = rd_word; rd_word='';}else if(!is_name)rd_key = string(cmd_data[?"_"]);
            rd_word='';
        }
     }
     rd_on=false;
  }
  
  // Reading
  if rd_on rd_word+=rd_char;
  // Submiting
  if is_done {
     if(is_subcmd){
        if(skip_cmd_at!=0){
        cmd_return='';
        if(looping>1){
          if(return_to_var!=''){cmd_vars[?return_to_var]=0; while(cmd_vars[?return_to_var]++<looping)u2d_cmd_handler(rd_word); ds_map_delete(cmd_vars,return_to_var); return_to_var=''}
          else repeat(looping)u2d_cmd_handler(rd_word);
          } else u2d_cmd_handler(rd_word);
        if(!is_val)is_subcmd=false; rd_word = cmd_return;
        } if(skip_cmd_at>=0)skip_cmd_at--;
     }
     
     if(statement!=''){
        switch(statement){
          case 'if':
            if(wait_for_value){ skip_cmd_at = string(rd_word)=='1'; wait_for_value=false;}
            statement='';
          break;
          case 'loop':
            if(wait_for_value){ looping = real(string_digits(rd_word)); wait_for_value=false;}
          break;
          default: break;
        }
     }
     if(is_name && !is_wvar){cmd_data[?"_"]=rd_word; is_name=false;}
     else if(is_str && is_rvar){rd_buffer+=cmd_vars[?rd_word]; is_rvar=false;} else
     if is_wvar{
       if is_val {
          if(is_subcmd){show_message("Variable Write:# "+string(rd_key)+" = CMD:"+string(rd_word)); is_subcmd=false;}
          else if(is_rvar){cmd_vars[?rd_key] = cmd_vars[?rd_word]; is_rvar=false;}
          else cmd_vars[?rd_key] = rd_word;
          is_val=false;
          }
       is_wvar=false;
     }else
     if(is_flag){
       if(is_flag==1){
         if not is_val rd_key=rd_word;
         var flags_len = string_length(rd_key);
         for(var i=1;i<=flags_len;i++){
           if(is_val){
                if(is_rvar){cmd_data[?"f."+string_char_at(rd_key,i)]=cmd_vars[?rd_word];}
                else if(is_subcmd)cmd_data[?"f."+string_char_at(rd_key,i)]=cmd_return;
                else cmd_data[?"f."+string_char_at(rd_key,i)]=rd_word;
           } else cmd_data[?"f."+string_char_at(rd_key,i)]=true;
         }
       }else if(is_flag==2){
          if not is_val rd_key=rd_word;
          if(is_val){
            if(is_rvar){cmd_data[?"f."+rd_key]=cmd_vars[?rd_word];}
            else if(is_subcmd){cmd_data[?"f."+rd_key]=cmd_return;}
            else cmd_data[?"f."+rd_key]=rd_word;
            is_val=false;
          } else cmd_data[?"f."+rd_key]=true;
       }
       if(is_rvar)is_rvar=false;
       if(is_subcmd)is_subcmd=false;
       is_flag=0;
     }else{
       if is_val {
            if(is_rvar){cmd_data[?rd_key]=cmd_vars[?rd_word]; is_rvar=false;}
            else if(is_subcmd){cmd_data[?rd_key]=cmd_return; is_subcmd=false;}
            else cmd_data[?rd_key]=rd_word;
            is_val=false;
       } else {
          if is_rvar{cmd_data[?"arg"+string(cmd_args++)]=cmd_vars[?rd_word]; is_rvar=false;}
          else if(is_subcmd){cmd_data[?"arg"+string(cmd_args++)]=cmd_return; is_subcmd=false;}
          else cmd_data[?"arg"+string(cmd_args++)]=rd_word;
       }
     }
     if is_end break;
     rd_word = ''; is_done=false;
  }
}

// Command Exec
if(ds_map_exists(cmd_data,'_')){
  var cmd_size = ds_map_size(cmd_commands);
  var cmd_item = cmd_data[?"_"];
  var is_cmd = false;
  if is_string(cmd_item) && string_char_at(cmd_commands[?cmd_item],1) == "~" {
    cmd_item = string_delete(cmd_commands[?cmd_item],1,1)
    is_cmd = true;
  }
  if is_cmd u2d_cmd_handler(cmd_item); else {
    while(is_string(cmd_item)){
      cmd_data[?"_"] = cmd_item;
      cmd_data[?"argc"] = cmd_args;
      cmd_item = cmd_commands[?cmd_item];
    }
    if(ds_map_exists(cmd_commands,cmd_data[?"_"])){
    if(script_exists(cmd_item)){
      script_execute(cmd_item,cmd_data);
      if(return_to_var!=''){cmd_vars[?return_to_var] = cmd_return; return_to_var='';}
    } else u2d_cmd_print("[definition error]"); }
  }
}
if not cmd_savedata ds_map_destroy(cmd_data);
return cmd_return;

#define u2d_cmd_add_command
/// u2d_cmd_add_command(command,func/alias)
//if(ds_exists(cmd_commands,ds_type_map)){
    ds_map_add(cmd_commands,argument0,argument1);
//}

#define u2d_cmd_print
if(cmd_print!=noone)if(script_exists(cmd_print))
 script_execute(cmd_print,argument0);
 else show_debug_message(argument0);
else show_debug_message(argument0);

#define u2d_cmd_value
/// u2d_cmd_value( string/real )
if(is_string(argument0)){
  if(string_length(string_letters(argument0))==0)
  return real(argument0);
  else return argument0;
}else return argument0;
#define u2d_cmd_var_set
// u2d_cmd_set_variable(name,value);
ds_map_replace(cmd_vars,string(argument0),argument1);

#define u2d_cmd_var_get
return ds_map_find_value(cmd_vars,string(argument0));

#define u2d_cmd_var_exists
return ds_map_exists(cmd_vars,string(argument0));

#define u2d_cmd_exists
/// u2d_cmd_exists(arg);
return ds_map_exists(cmd_data,string(argument0));
#define u2d_cmd_autocompl
/// u2d_cmd_autocomplite(input)
var compl = '';
if(string_length(argument0) == 0) return '';
var cmd_parts = string_split(string(argument0),' ');
var last = ds_map_find_last(cmd_commands);

// search cmd triger
if !is_array(cmd_parts)cmd_part=cmd_parts; else return '';

// get full command name
var len = string_length(cmd_part);
if( cmd_part == string_copy(last,0,len) ) compl=last; else
  for(c=ds_map_find_first(cmd_commands);c!=last;c=ds_map_find_next(cmd_commands,c)){
    if(cmd_part == string_copy(c,0,len)){compl=c; break;}
  }
  
return string_trim(compl);
