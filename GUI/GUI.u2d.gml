#define u2d_gui_init
///u2d_gui_init() Use object as GUI

// System
__GUI_WIDGETS_PRIORITY = ds_priority_create();
__GUI_WIDGETS_PRIORITY_HANDLER = ds_priority_create();
__GUI_DND_ID = noone;
__GUI_DND_HANDLER = noone;
__GUI_DND_CURSOR[0] = 0;
__GUI_DND_CURSOR[1] = 0;

// Widgets Priority
P_DND = 10000;
P_WIDGET = 1000;
P_LAYOUT = 0;

// Widgets States
enum state {
  normal      = 0,
  hover       = 1,
  clicked     = 2,
  focus       = 3,
  disabled    = 4,
  deactive    = 5,
}

enum layout {
  canvas      = 0,
  dock        = 1,
  grid        = 2,
  hstack      = 3,
  vstack      = 4,
  hwarp       = 5,
  vwarp       = 6,
}

enum align {
  left        = 0,
  top         = 0,
  center      = 1,
  middle      = 1,
  right       = 2,
  bottom      = 2,
  fill        = 3,
}

#define u2d_gui_widget
///u2d_gui_widget()
var new_widget = ds_map_create();
ds_priority_add(__GUI_WIDGETS_PRIORITY,new_widget,P_WIDGET);
ds_map_add(new_widget,"priority",P_WIDGET);

// Basic
ds_map_add(new_widget,"x",0);              // X-pos
ds_map_add(new_widget,"y",0);              // Y-pos
ds_map_add(new_widget,"rw",48);            // X-pos
ds_map_add(new_widget,"rh",48);            // Y-pos
ds_map_add(new_widget,"val",'');           // Value

// System
ds_map_add(new_widget,"state",state.normal);     // State
ds_map_add(new_widget,"skip_update",false);        // State
ds_map_add(new_widget,"check",false);              // Checked
ds_map_add(new_widget,"subtype",'W');              // typeof

// Drawing
ds_map_add(new_widget,"objXY",false);       // From object XY
ds_map_add(new_widget,"w",-1);              // Width
ds_map_add(new_widget,"h",-1);              // Heigth
ds_map_add(new_widget,"min-w",-1);          // Min Width
ds_map_add(new_widget,"min-h",-1);          // Min Heigth
ds_map_add(new_widget,"max-w",-1);          // Max Width
ds_map_add(new_widget,"max-h",-1);          // Max Heigth
ds_map_add(new_widget,"halign",align.left); // Horz Align
ds_map_add(new_widget,"valign",align.top);  // Vert Align

// Actions
ds_map_add(new_widget,"a_check",false);     // Checkable
ds_map_add(new_widget,"a_focus",false);     // Focusable
ds_map_add(new_widget,"a_textedit",false);  // Editable

// Text
ds_map_add(new_widget,"text",false);        // Show text
ds_map_add(new_widget,"length",100);        // Mex Length
ds_map_add(new_widget,"password",false);    // Hide chars
ds_map_add(new_widget,"curpos",0);          // Cursor Position

// DnD
ds_map_add(new_widget,"dnd",false);         // Using Drag&Drop
ds_map_add(new_widget,'dnd-grid-x',1);      // Grid Move OX
ds_map_add(new_widget,'dnd-grid-y',1);      // Grid Move OY


return new_widget;

#define u2d_gui_update
///u2d_gui_update()
if !visible return 0;

u2d_gui_draw_begin();
__GUI_DND_HANDLER = noone;
while(ds_priority_size(__GUI_WIDGETS_PRIORITY_HANDLER)!=0){
  var item = ds_priority_delete_max(__GUI_WIDGETS_PRIORITY_HANDLER);
  
  var item_state = ds_map_find_value(item,"state");
  
  // widget skip
  if(item_state == state.deactive || item_state == state.disabled || item[?"skip_update"]){
  continue;}
  
  if(item[?"w"] != -1)item[?"rw"]=item[?"w"]; else{
    if(item[?"min-w"] !=-1 && item[?"rw"]<item[?"min-w"])item[?"rw"]=item[?"min-w"];
    if(item[?"max-w"] !=-1 && item[?"rw"]>item[?"max-w"])item[?"rw"]=item[?"max-w"];
  }
  if(item[?"h"] != -1)item[?"rh"]=item[?"h"];else{
    if(item[?"min-h"] !=-1 && item[?"rh"]<item[?"min-h"])item[?"rh"]=item[?"min-h"];
    if(item[?"max-h"] !=-1 && item[?"rh"]>item[?"max-h"])item[?"rh"]=item[?"max-h"];
  }
  
  
  var _x = item[?"x"];
  var _y = item[?"y"];
  var _w = item[?"rw"];
  var _h = item[?"rh"];
  if(item[?"objXY"]){_x+=x; _y+=y;}
  //if(ds_map_exists(item,'margin.left')){_x+=item[?"margin.left"];}
  //if(ds_map_exists(item,'margin.top')){_y+=item[?"margin.top"];}
  //if(ds_map_exists(item,'margin.right')){_w-=item[?"margin.right"]; }
  //if(ds_map_exists(item,'margin.bottom')){_h-=item[?"margin.bottom"];}
  
  var is_hover = u2d_gui_mouseon_rect(_x,_y,_x+_w,_y+_h);
  if(__GUI_DND_ID!=noone){if(__GUI_DND_ID!=item){
      if(is_hover && __GUI_DND_HANDLER==noone)__GUI_DND_HANDLER = item;
      continue;
  }}
  
  if(item[?"dnd"]){
      if(is_hover && device_mouse_check_button_pressed(0,mb_left) && __GUI_DND_ID!=item){
           __GUI_DND_ID = item;
           __GUI_DND_CURSOR[0] = device_mouse_x(0)-_x;
           __GUI_DND_CURSOR[1] = device_mouse_y(0)-_y;
           u2d_gui_event_call(item,'dnd.grab');
           if(item[?"subtype"] != 'L')ds_priority_change_priority(__GUI_WIDGETS_PRIORITY,item,P_DND);
           }
      if(device_mouse_check_button(0,mb_left) && __GUI_DND_ID=item){
        if(item[?"dnd-grid-x"]!=0) _x = device_mouse_x(0)-__GUI_DND_CURSOR[0];
        if(item[?"dnd-grid-y"]!=0) _y = device_mouse_y(0)-__GUI_DND_CURSOR[1];
        
        if(item[?"dnd-grid-x"]>1) _x-=_x mod item[?"dnd-grid-x"];
        if(item[?"dnd-grid-y"]>1) _y-=_y mod item[?"dnd-grid-y"];
        
        
        item[?"x"] = _x;
        item[?"y"] = _y;
         if(item[?"objXY"]){
           item[?"x"]-=x;
           item[?"y"]-=y;
         }
         if(item[?"x"]<0)item[?"x"]=0; if(item[?"x"]+item[?"rw"]>room_width)item[?"x"]=room_width-item[?"rw"];
         if(item[?"y"]<0)item[?"y"]=0; if(item[?"y"]+item[?"rh"]>room_height)item[?"y"]=room_height-item[?"rh"];
         
         u2d_gui_event_call(item,'dnd.move');
         continue
      } else if(__GUI_DND_ID==item){ds_priority_change_priority(__GUI_WIDGETS_PRIORITY,item,item[?"priority"]); u2d_gui_event_call(item,'dnd.drop'); __GUI_DND_HANDLER=noone; __GUI_DND_ID=noone;}
  }
  
  if(is_hover){
    if(item_state == state.normal){u2d_gui_event_call(item,'hover'); item_state = state.hover;}
  } else if(item_state == state.hover){u2d_gui_event_call(item,'leave'); item_state = state.normal;}
  
  if(device_mouse_check_button_pressed(0,mb_left) && item_state == state.hover){u2d_gui_event_call(item,'click'); item_state = state.clicked;}
  
  if(item_state == state.clicked){
    if(device_mouse_check_button_released(0,mb_left))
    if(is_hover){
      if(item[?"a_focus"]){u2d_gui_event_call(item,'focus'); item_state = state.focus; if(item[?"a_textedit"]){keyboard_string=''; item[?"curpos"]=string_length(item[?"val"]);}} else
      if(item[?"a_check"]){u2d_gui_event_call(item,'check'); item[?"check"]=!item[?"check"]; item_state = state.normal;} 
      else {u2d_gui_event_call(item,'clicked'); item_state = state.normal;}
    } else item_state = state.normal;
    //else item_state = state.normal;
  }
  
  if(item_state==state.focus && device_mouse_check_button_released(0,mb_left) && !is_hover){u2d_gui_event_call(item,'blur'); item_state=state.normal;}
  
  if(item_state==state.focus && item[?"a_textedit"]){
    var value = string(item[?"val"]);
    var curpos = item[?"curpos"];
    var is_combo = false;
    if(keyboard_string != '' && keyboard_check_pressed(vk_anykey)){ value = string_insert(keyboard_string,value,curpos+1); curpos+=string_length(keyboard_string); keyboard_string=''; }  
    if(keyboard_check_pressed(vk_left) && curpos>0)curpos--;
    if(keyboard_check_pressed(vk_right) && curpos<string_length(value))curpos++;
    if(keyboard_check_pressed(vk_home))curpos=0;
    if(keyboard_check_pressed(vk_end))curpos=string_length(value);
    if(keyboard_check_pressed(vk_backspace) && curpos>0){value = string_delete(value,curpos,1); curpos--;}
    if(keyboard_check_pressed(vk_delete) && curpos<string_length(value)){value = string_delete(value,curpos+1,1);}
    if(keyboard_check(vk_control) && keyboard_check_pressed(ord('V'))){var clipboard=clipboard_get_text(); value = string_insert(clipboard,value,curpos+1); curpos+=string_length(clipboard);}
    if(string_length(value) > item[?"length"]) value=string_copy(value,1,item[?"length"]);
    item[?"val"]=value; item[?"curpos"]=min(curpos,string_length(value));
  }
  
  if(item_state != item[?"state"]){u2d_gui_event_call(item,'state'); item[?"state"]=item_state; break;}
}

u2d_gui_draw_begin();
while(ds_priority_size(__GUI_WIDGETS_PRIORITY_HANDLER)!=0){
    var item = ds_priority_delete_min(__GUI_WIDGETS_PRIORITY_HANDLER);
    if(item[?"subtype"]=='L')u2d_gui_layout_update(item);
}

#define u2d_gui_layout
///u2d_gui_layout(widget, type)

argument0[?"subtype"]='L';
argument0[?"layout"]=real(argument1);
argument0[?"items"]=ds_list_create();

ds_map_replace(argument0,"priority",P_LAYOUT);
ds_priority_change_priority(__GUI_WIDGETS_PRIORITY,argument0,P_LAYOUT);

if(argument1 == layout.grid){argument0[?"rows"]=2; argument0[?"cols"]=2;}
return argument0;

#define u2d_gui_mouseon_rect
/// u2d_gui_mouseon_rect(x1,y1,x2,y2);
var xx = device_mouse_x(0);
var yy = device_mouse_y(0);
return (xx>=argument0 && xx<=argument2) && (yy>=argument1 && yy<=argument3);

#define u2d_debug_draw
/// u2d_debug_draw(widget);

var debug_info = 
"Widget Debuging#"
+"DND: "+string(__GUI_DND_ID)+'#'
+"----------------------------#"
+"X-Pos: "+string(argument0[?"x"])+"px#"
+"Y-Pos: "+string(argument0[?"y"])+"px#"
+"Real-W: "+string(argument0[?"rw"])+"px#"
+"Real-H: "+string(argument0[?"rh"])+"px#"
+"------------------------#"
+"State: "+string(argument0[?"state"])+"#"
+"Checked: "+string(argument0[?"check"])+"#"
+"Tab: "+string(argument0[?"tab"])+"#"
+"----------------------------#"
+"UseObjectXY: "+string(argument0[?"objXY"])+"#"
+"Static-W: "+string(argument0[?"w"])+"#"
+"Static-H: "+string(argument0[?"h"])+"#"
+"Min-W: "+string(argument0[?"min-w"])+"#"
+"Min-H: "+string(argument0[?"min-h"])+"#"
+"Max-W: "+string(argument0[?"max-w"])+"#"
+"Max-H: "+string(argument0[?"max-h"])+"#"
+"----------------------------#"
+"Checkable: "+string(argument0[?"a_check"])+"#"
+"Focusable: "+string(argument0[?"a_focus"])+"#"
+"Editable: "+string(argument0[?"a_textedit"])+"#"
+"----------------------------#"
+"Value:#"+string(argument0[?"val"])+"#"

var xx=argument0[?"x"];
var yy=argument0[?"y"];
if(argument0[?"objXY"]){xx+=x; yy+=y;}
draw_rectangle_colour(xx,yy,xx+argument0[?"rw"],yy+argument0[?"rh"],c_blue,c_blue,c_blue,c_blue,false);
if(argument0[?"text"]) draw_text(xx+5,yy+5,argument0[?"val"]);

draw_set_alpha(.8);
draw_rectangle_colour(0,0,string_width(debug_info)+20,room_height,c_black,c_black,c_black,c_black,false);
draw_set_alpha(1);
draw_text_colour(5,5,debug_info,c_white,c_white,c_white,c_ltgray,1);

#define u2d_gui_layout_add_widget
/// u2d_gui_layout_add_widget( layout, widget )
if(argument0[?"subtype"] != 'L')return 0;
if(ds_list_find_index(argument0[?"items"],argument1) != -1) return 0;
ds_list_add(argument0[?"items"],argument1); argument1[?"par"] = argument0;
if(argument1[?"subtype"] == 'L'){
  argument1[?"priority"]=argument0[?"priority"]+1;
  ds_priority_change_priority(__GUI_WIDGETS_PRIORITY,argument1,argument1[?"priority"]);
}

#define u2d_gui_layout_update
/// u2d_gui_layout_update( layout )
if(argument0[?"subtype"]!='L')return 0;

var items_count = ds_list_size(argument0[?"items"]);
if(items_count == 0)return 0;

switch(argument0[?"layout"]){
  case layout.canvas:
       for(var i=0;i<items_count;i++){
           var item = ds_list_find_value(argument0[?"items"],i);
           if(item[?"state"] == state.deactive || __GUI_DND_ID==item)continue;
           
           if(ds_map_exists(item,"canvas.left")) item[?"x"] = argument0[?"x"] + item[?"canvas.left"]; else item[?"x"] = argument0[?"x"];
           
           if(ds_map_exists(item,"canvas.top")) item[?"y"] = argument0[?"y"] + item[?"canvas.top"]; else item[?"y"] = argument0[?"y"];
           
           if(ds_map_exists(item,"canvas.right")){
             var ww = argument0[?'x']+argument0[?"rw"] - item[?"canvas.right"];
             if(ds_map_exists(item,"canvas.left")) item[?"rw"] = ww - item[?'x'];
             else item[?"x"] = ww - item[?"rw"];
           }
           
           if(ds_map_exists(item,"canvas.bottom")){
             var hh = argument0[?'y']+argument0[?"rh"] - item[?"canvas.bottom"];
             if(ds_map_exists(item,"canvas.top")) item[?"rh"] = hh - item[?"y"];
             else item[?"y"] = hh - item[?"rh"];
           }
       }
  break;
  case layout.dock:
       var fill_top = 0;
       var fill_bottom = 0;
       var fill_left = 0;
       var fill_right = 0;
       
       var area_x = 0;
       var area_y = 0;
       var area_w = 0;
       var area_h = 0;
       
       for(var i=0;i<items_count;i++){
           var item = ds_list_find_value(argument0[?"items"],i);
           if(item[?"state"] == state.deactive)continue;
           
           switch (ds_map_find_value(item,'dock'))
           {
               case "dock.top":
                    area_x = argument0[?"x"] + fill_left;
                    area_y = argument0[?"y"] + fill_top;
                    area_w = argument0[?"rw"] - fill_left - fill_right;
                    area_h = item[?"rh"]
                    if(item[?"w"]>area_w)item[?"rw"] = area_w;
                    item[?"y"]=area_y;
                    fill_top+=item[?"rh"]+1;
               break;
               case "dock.bottom":
                    fill_bottom += item[?"rh"]+1;
                    area_x = argument0[?"x"] + fill_left;
                    area_y = argument0[?"y"] + argument0[?"rh"] - fill_bottom;
                    area_w = argument0[?"rw"] - fill_left - fill_right;
                    area_h = item[?"rh"]
                    if(item[?"rw"]>area_w)item[?"rw"] = area_w;
                    item[?"y"]=area_y;
               break;
               case "dock.left":
                    area_x = argument0[?"x"] + fill_left;
                    area_y = argument0[?"y"] + fill_top;
                    area_w = item[?"rw"];
                    area_h = argument0[?"rh"] - fill_bottom - fill_top;
                    if(item[?"rh"]>area_h)item[?"rh"] = area_h;
                    item[?"y"]=area_y;
                    fill_left+=item[?"rw"]+1;
               break;
               case "dock.right":
                    fill_right+=item[?"rw"]+1;
                    area_x = argument0[?"x"] + argument0[?"rw"] - fill_right;
                    area_y = argument0[?"y"] + fill_top;
                    area_w = item[?"rw"];
                    area_h = argument0[?"rh"] - fill_bottom - fill_top;
                    if(item[?"rh"]>area_h)item[?"rh"] = area_h;
                    item[?"y"]=area_y;
               break;
               case "dock.fill":
                    area_x = argument0[?"x"] + fill_left;
                    area_y = argument0[?"y"] + fill_top;
                    area_w = argument0[?"rw"] - fill_right -fill_left;
                    area_h = argument0[?"rh"] - fill_top - fill_bottom;
               break;
           }
           
           if(__GUI_DND_ID==item) continue;
           
           switch (item[?"halign"])
           {
               case align.left: item[?"x"]= area_x; break;
               case align.right: item[?"x"]= area_x+area_w-item[?"rw"]; break;
               case align.center: item[?"x"]= area_x+floor(area_w/2)-floor(item[?"rw"]/2); break;
               case align.fill: item[?"x"]= area_x; item[?"rw"]=area_w; break;
           }
           
           switch (item[?"valign"])
           {
               case align.top: item[?"y"]= area_y; break;
               case align.bottom: item[?"y"]= area_y+area_h-item[?"rh"]; break;
               case align.center: item[?"y"]= area_y+floor(area_h/2)-floor(item[?"rh"]/2); break;
               case align.fill: item[?"y"]= area_y; item[?"rh"]=area_h; break;
           }
           if(ds_map_exists(item,'margin.left')){item[?"x"]+=item[?"margin.left"];}
           if(ds_map_exists(item,'margin.top')){item[?"y"]+=item[?"margin.top"];}
           if(ds_map_exists(item,'margin.right')){item[?"rw"]-=item[?"margin.right"]; }
           if(ds_map_exists(item,'margin.bottom')){item[?"rh"]-=item[?"margin.bottom"];}
       }
       
  break;
  case layout.grid:
       var cols = argument0[?"cols"], rows = argument0[?"rows"];
       var wspace = argument0[?"rw"], unset_cols = cols;
       var hspace = argument0[?"rh"], unset_rows = rows;
       
       for(var i=0;i<cols;i++) if(ds_map_exists(argument0,"wcol-"+string(i))) {wspace-=argument0[?"wcol-"+string(i)]; unset_cols--;}
       for(var i=0;i<rows;i++) if(ds_map_exists(argument0,"hrow-"+string(i))) {hspace-=argument0[?"hrow-"+string(i)]; unset_rows--;}
       
       var wcol=wspace, hrow=hspace;
       
       if(ds_map_exists(argument0,"wcol")) wcol=argument0[?"wcol"];
       wcol = min(round(wspace/unset_cols),wcol);
       
       if(ds_map_exists(argument0,"hrow")) hrow=argument0[?"hrow"];
       hrow = min(round(hspace/unset_rows),hrow);
       
       for(var i=0;i<items_count;i++){
           var area_x = 0, area_y = 0, area_w = 0, area_h = 0;
           
           var item = ds_list_find_value(argument0[?"items"],i);
           if(item[?"state"] == state.deactive || __GUI_DND_ID==item)continue;
           
           var col = item[?"col"], row = item[?"row"];
           if(col > cols || row > rows){ item[?"state"]=state.deactive; continue; }
           
           var span_col=col, span_row=row;
           if(ds_map_exists(item,"hspan"))span_col+=item[?"hspan"]-1;
           if(ds_map_exists(item,"vspan"))span_row+=item[?"vspan"]-1;
           
           for(var c=col;c<=span_col;c++){
              if(ds_map_exists(argument0,"wcol-"+string(c)))
                   area_w += ds_map_find_value(argument0,"wcol-"+string(c));
              else area_w += wcol;
           }
           
           for(var r=row;r<=span_row;r++){
              if(ds_map_exists(argument0,"hrow-"+string(r)))
                   area_h += ds_map_find_value(argument0,"hrow-"+string(r));
              else area_h += hrow;
           }
           
           area_x = argument0[?"x"]; 
           if(col!=0){ for(var c=0;c<col;c++){
              if(ds_map_exists(argument0,"wcol-"+string(c)))
                   area_x += ds_map_find_value(argument0,"wcol-"+string(c));
              else area_x += wcol;
           } }
           
           area_y = argument0[?"y"];
           if(row!=0){for(var r=0;r<row;r++){
              if(ds_map_exists(argument0,"hrow-"+string(r)))
                   area_y += ds_map_find_value(argument0,"hrow-"+string(r));
              else area_y += hrow;
           } }
           
           switch (item[?"halign"])
           {
               case align.left: item[?"x"]= area_x; break;
               case align.right: item[?"x"]= area_x+area_w-item[?"rw"]; break;
               case align.center: item[?"x"]=area_x+floor(area_w/2)-floor(item[?"rw"]/2); break;
               case align.fill: item[?"x"]= area_x; item[?"rw"]=area_w; break;
           }
           
           switch (item[?"valign"])
           {
               case align.top: item[?"y"]=area_y; break;
               case align.bottom: item[?"y"]= area_y+area_h-item[?"rh"]; break;
               case align.center: item[?"y"]= area_y+floor(area_h/2)-floor(item[?"rh"]/2); break;
               case align.fill: item[?"y"]= area_y; item[?"rh"]=area_h; break;
           }
           
           if(ds_map_exists(item,'margin.left')){item[?"x"]+=item[?"margin.left"];}
           if(ds_map_exists(item,'margin.top')){item[?"y"]+=item[?"margin.top"];}
           if(ds_map_exists(item,'margin.right')){item[?"rw"]-=item[?"margin.right"]; }
           if(ds_map_exists(item,'margin.bottom')){item[?"rh"]-=item[?"margin.bottom"];}
       }
       
  break;
  case layout.hstack: 
       var stack_w = 0, sep = 0;
       if(ds_map_exists(argument0,"sep"))sep = argument0[?"sep"];
       var area_x,area_y,area_h;
       for(var i=0;i<items_count;i++){
           var item = ds_list_find_value(argument0[?"items"],i);
           if(item[?"state"] == state.deactive || __GUI_DND_ID==item)continue;
           item[?"x"] = argument0[?"x"]+stack_w+1; stack_w+=item[?"rw"]+sep+1;
           area_y = argument0[?"y"];
           area_h = argument0[?"rh"];
           
           if(__GUI_DND_ID==item) continue;
           switch (item[?"valign"])
           {
               case align.top: item[?"y"]= area_y; break;
               case align.bottom: item[?"y"]= area_y+area_h-item[?"rh"]; break;
               case align.center: item[?"y"]= area_y+floor(area_h/2)-floor(item[?"rh"]/2); break;
               case align.fill: item[?"y"]= area_y; item[?"rh"]=area_h; break;
           }
           if(ds_map_exists(item,'margin.left')){item[?"x"]+=item[?"margin.left"];}
           if(ds_map_exists(item,'margin.top')){item[?"y"]+=item[?"margin.top"];}
           if(ds_map_exists(item,'margin.right')){item[?"rw"]-=item[?"margin.right"]; }
           if(ds_map_exists(item,'margin.bottom')){item[?"rh"]-=item[?"margin.bottom"];}
       }
  break;
  case layout.vstack:
       var stack_h = 0, sep = 0;
       if(ds_map_exists(argument0,"sep"))sep = argument0[?"sep"];
       var area_x,area_y,area_w;
       for(var i=0;i<items_count;i++){
           var item = ds_list_find_value(argument0[?"items"],i);
           if(item[?"state"] == state.deactive)continue;
           area_x = argument0[?"x"];
           item[?"y"] = argument0[?"y"]+stack_h+1; stack_h+=item[?"rh"]+sep+1;
           area_w = argument0[?"rw"];
           if(__GUI_DND_ID==item) continue;
           switch (item[?"halign"])
           {
               case align.left: item[?"x"]= area_x; break;
               case align.right: item[?"x"]= area_x+area_w-item[?"rw"]; break;
               case align.center: item[?"x"]= area_x+floor(area_w/2)-floor(item[?"rw"]/2); break;
               case align.fill: item[?"x"]= area_x; item[?"rw"]=area_w; break;
           }
           if(ds_map_exists(item,'margin.left')){item[?"x"]+=item[?"margin.left"];}
           if(ds_map_exists(item,'margin.top')){item[?"y"]+=item[?"margin.top"];}
           if(ds_map_exists(item,'margin.right')){item[?"rw"]-=item[?"margin.right"]; }
           if(ds_map_exists(item,'margin.bottom')){item[?"rh"]-=item[?"margin.bottom"];}
       }
  break;
  case layout.hwarp:
       var Hline = 0;
       var Wline = 0;
       var Hall = 0;
       for(var i=0;i<items_count;i++){
           var item = ds_list_find_value(argument0[?"items"],i);
           if(item[?"state"] == state.deactive)continue;
           var sep=0; if( ds_map_exists(argument0,"sep") )sep=argument0[?"sep"];
           if(Wline+item[?"rw"]>argument0[?"rw"]){Wline=0; Hall+=Hline+sep+1; Hline=0;}
           if(__GUI_DND_ID!=item){item[?"x"]=Wline; item[?"y"]=argument0[?"y"]+Hall;}
           Wline+=item[?"rw"]+sep+1;
           Hline=max(item[?"rh"],Hline);
       }
  break;
  case layout.vwarp: break;
}

#define u2d_gui_event_bind
/// u2d_gui_event_bind( widget, event, "act:src")
ds_map_add(argument0,'ev'+string(argument1),string(argument2));



#define u2d_gui_event_call
/// u2d_gui_event_call( widget, event );
argument1 = 'ev'+argument1;
if(!ds_map_exists(argument0,argument1))return 0;
var event_data = string_split(ds_map_find_value(argument0,argument1),':');
if not is_array(event_data) return 0;
switch (event_data[0])
{
    case "script":
         var script_data = string_split(event_data[1],',');
         if( is_array(script_data) ){
           if(asset_get_type(script_data[0]) != asset_script)return 0;
           var script = asset_get_index(script_data[0]);
           var data_length = array_length_1d(script_data);
           var args;
           for(var i=0;i<5;i++){
             if(i<data_length-1){
               args[i] = script_data[i+1];
             } else args[i]="";
           }
           script_execute(script,argument0,args[0],args[1],args[2],args[3],args[4]);
         }
         else {
           if(asset_get_type(script_data) != asset_script)return 0;
           var script = asset_get_index(script_data);
           script_execute(script,argument0);
         }
    break;
    case "event":
         var e_data = string_split(event_data[1],',');
         if( is_array(e_data) ){
           event_perform( real(e_data[0]), real(e_data[1]) );
         }
    break;
}
 

#define u2d_gui_draw_begin
/// u2d_gui_draw_begin()
ds_priority_clear(__GUI_WIDGETS_PRIORITY_HANDLER);
ds_priority_copy(__GUI_WIDGETS_PRIORITY_HANDLER,__GUI_WIDGETS_PRIORITY);

#define u2d_gui_draw_get_widget
/// u2d_gui_draw_get_widget()
if(ds_priority_size(__GUI_WIDGETS_PRIORITY_HANDLER) == 0)return noone;
var widget = ds_priority_delete_min(__GUI_WIDGETS_PRIORITY_HANDLER);
if(widget[?"state"] == state.deactive) return -1; return widget;

#define u2d_gui_layout_deactive
/// u2d_gui_layout_deactive(layout)

if(argument0[?"state"]!=state.deactive)argument0[?"state"]=state.deactive; else argument0[?"state"]=state.normal;
var count = ds_list_size(argument0[?"items"]);
if(count == 0)return 0;
for(var i=0;i<count;i++){
  var item = ds_list_find_value(argument0[?"items"],i);
  if(item[?"subtype"]=='L')u2d_gui_layout_deactive(item); else item[?"state"]=argument0[?"state"];
}

#define u2d_gui_get_text
/// u2d_gui_get_text(widget)
if(!argument0[?"text"])return "";
var text = argument0[?"val"];

var curpos=argument0[?"curpos"] ,max_length = 0,tmp_string='';
while(string_width_ext(tmp_string,-1,-1) < argument0[?"rw"])tmp_string+='W'; max_length = string_length(tmp_string);

if(argument0[?"password"]==true){
   tmp_string = "";
   for(i=0;i<string_length(text)-1;i++)tmp_string+='*';
   text = tmp_string;
}

if(string_length(text) > max_length){
    var max_len = max_length;
    if not(argument0[?"a_textedit"] && argument0[?"state"]==state.focus) curpos=string_length(text)-1;
    var tmpL=curpos,tmpR=curpos;
    while(max_len>0){
        if(tmpL>0){tmpL--; max_len--;}
        if(max_len==0)break;
        if(tmpR<string_length(text)-1){tmpR++; max_len--;}
    }
    text = string_copy(text,tmpL,tmpR);
}

if(argument0[?"a_textedit"] && argument0[?"state"]==state.focus)text=string_insert('|',text,curpos+1);

return text;
