![U2D logo.jpg](https://bitbucket.org/repo/B5keMq/images/1002090480-U2D%20logo.jpg)

Разработчик: SpireX

Среда разработки: Game Maker Studio 1.4+

Язык программирования: Game Maker Language (GML)

### Описание ###
Ultra2D - это бесплатный набор сценариев для среды Game Maker Studio, которые просто помогают решать задачи, что встречаются почти в каждом проекте.

Все сценарии написанны на основном языке GMS - Game Maker Language (GML) без использования сторонних библиотек. Поэтому используя данный набор, проект не потеряет особенности GML (например кроссплатформенность).

### Что входит в Ultra2D? ###
В данный момент в Ultra2D входят сценарии:

* Animation - для работы с анимацией спрайтов.
* Camera - для создания управляемой камеры.
* Console - обработчик внутренеигровой командной строки
* GUI - поможет разработать пользовательский интерфейс в игре
* Menu - для создания игрового меню с легкой настройкой
* GML+ - дополнительные функции GML которых мне ОЧЕНЬ не хватало.

Набор Ultra2D часто обновляется!