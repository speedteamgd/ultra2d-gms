#define string_fill
/// string_fill( text, len, char);
if !is_string(argument0)argument0=string(argument0);
while(string_length(argument0) < argument1)argument0=argument2+argument0; return argument0;
#define string_make
/// string_make(sep, val1, val2, ...)
if(argument_count<2)return "";
var outString = "";
for(var i=1;i<argument_count;i++){
  outString+=string(argument[i])
  if(i<argument_count-1)outString+=argument[0];
}
return outString;
#define string_split
/// string_split( str, chr )
if(string_count(argument1,argument0) == 0)return argument0;

var result, point = 0, length,find=0;
length = string_length(argument0)
for(var i=0;i<=length;i++){
  var char = string_char_at(argument0,i+1);
  if(char == argument1 || i==length){
     result[find++]=string_trim(string_copy(argument0,point+1,i-point),' ');
     point=i+1;
     }
}
if(array_length_1d(result) == 1) return result[0]; else return result;
#define string_trim
/// string_trim(string [,char])
if(!is_string(argument[0]) || argument_count==0)return "";
var char = ' ',str = argument[0],len = string_length(str);
if(argument_count == 2)char = argument[1];
var trim = 0;
// Trim-start
for(var i=1; i<len;i++){
  var t_char = string_char_at(str,i);
  if(t_char == char)trim++; else break;
}
if(trim!=0){str = string_copy(str,trim+1,len-trim); len = string_length(str); trim=0;}
// Trim-end
for(var i=0; i<len;i++){
  var t_char = string_char_at(str,len-i);
  if(t_char == char)trim++; else break;
}
if(trim!=0)str = string_copy(str,0,len-trim);

return str;
#define u2d_color_get_brighting
// u2d_draw_get_color_lighten( color, count);
if(argument1==0)return argument0;
var value = colour_get_value(argument0) + argument1;
if(value<0) value=0; else if(value>255) value=255;
return make_colour_hsv(colour_get_hue(argument0), colour_get_saturation(argument0), value);
#define u2d_color_get_mix
/// u2d_color_mix( col1, col2, ...);
if(argument_count == 0) return 0;
if(argument_count == 1) return argument[0];
var r = colour_get_red(argument[0]),g = colour_get_green(argument[0]),b = colour_get_blue(argument[0]);
for(var i=1;i<argument_count;i++){
   r = floor((colour_get_red(argument[i])+r)/2);
   g = floor((colour_get_green(argument[i])+g)/2);
   b = floor((colour_get_blue(argument[i])+b)/2);
}
return make_colour_rgb(r,g,b);
#define u2d_draw_background_tiled_h
/// u2d_draw_background_tiled_h(background, x, y, xscale, yscale, rot, color, alpha, sep);
var background_start, background_end, background_current, background_step;
       
background_step  = background_get_width(argument0)+argument8;
background_start = view_xview+((argument1-view_xview) mod background_step)-background_step;
background_end   = view_xview+view_wview+background_step;

for (background_current = background_start; background_current <= background_end; background_current += background_step)
draw_background_ext(argument0, background_current, argument2, argument3, argument4, argument5, argument6, argument7);
#define u2d_draw_background_tiled_h_part
/// u2d_draw_background_tiled_h_part(background, left, top, width, height, x, y, sep, xscale, yscale, color, alpha);

var background_start, background_end, background_current, background_step;     
background_step  = argument3+argument7;
background_start = view_xview+((argument5-view_xview) mod background_step)-background_step;
background_end   = view_xview+view_wview+background_step;
    
for (background_current = background_start; background_current <= background_end; background_current += background_step)
draw_background_part_ext(argument0, argument1, argument2, argument3, argument4, background_current, argument6, argument8, argument9, argument10, argument11);
#define u2d_draw_background_tiled_v
/// u2d_draw_background_tiled_v(background, x, y, xscale, yscale, rot, color, alpha, sep);
var background_start, background_end, background_current, background_step;
       
background_step  = background_get_height(argument0)+argument8;
background_start = view_yview+((argument2-view_yview) mod background_step)-background_step;
background_end   = view_yview+view_hview+background_step;

for (background_current = background_start; background_current <= background_end; background_current += background_step)
draw_background_ext(argument0, argument1, background_current, argument3, argument4, argument5, argument6, argument7);
#define u2d_draw_background_tiled_v_part
/// u2d_draw_background_tiled_v_part(background, left, top, width, height, x, y, sep, xscale, yscale, color, alpha);

var background_start, background_end, background_current, background_step;     
background_step  = argument4+argument7;
background_start = view_yview+((argument6-view_yview) mod background_step)-background_step;
background_end   = view_yview+view_hview+background_step;

for (background_current = background_start; background_current <= background_end; background_current += background_step)
draw_background_part_ext(argument0, argument1, argument2, argument3, argument4, argument5, background_current, argument8, argument9, argument10, argument11);
#define u2d_draw_rectangle_rounded
/// u2d_draw_rectangle_rounded(x,y,w,h,r,outline)
var type; if(argument5 == true) type = pr_linestrip; else type = pr_trianglefan;
draw_primitive_begin(type)
  for(var i=0;i<=360;i+=5){
    var xx,yy;
    if(i<=180 || i==360) yy = argument1 + argument4; else yy = argument1 + argument3 - argument4;
    if(i>=90 && i<270) xx = argument0 + argument4; else xx = argument0 + argument2 - argument4;
    draw_vertex(xx+lengthdir_x(argument4,i),yy+lengthdir_y(argument4,i))
  }
draw_primitive_end();
#define u2d_draw_rectangle_rounded_color
/// u2d_draw_rectangle_rounded_color(x,y,w,h,c1,c2,c3,c4,r,s,outline)
var type; if(argument10) type = pr_linestrip; else type = pr_trianglefan;
if(argument9>90)argument9=90;
draw_primitive_begin(type)
  var xx=0, yy=0,xo,yo,c=0,d=0;
  if(!argument10)draw_vertex_colour(argument0+argument2/2,argument1+argument3/2,u2d_color_get_mix(argument4,argument5,argument6,argument7),1);
  for(part=0;part<=4;part++){
    xo=xx; yo=yy;
    switch (part)
    {
        case 1: d=90; xx=argument0+argument8; yy=argument1+argument8; c=argument5; break;
        case 2: d=180; xx=argument0+argument8; yy=argument1+argument3-argument8; c=argument6; break;
        case 3: d=270;xx=argument0+argument2-argument8; yy=argument1+argument3-argument8; c=argument7; break;
        default: d=0; xx=argument0+argument2-argument8; yy=argument1+argument8; c=argument4; break;
    } 
    
    if(part!=4)for(var deg=0;deg<=90;deg+=argument9){
      draw_vertex_colour(xx + lengthdir_x(argument8,deg+d),yy + lengthdir_y(argument8,deg+d),c,1);
    }else draw_vertex_colour(xx + argument8,yy,c,1);
  }
draw_primitive_end();
#define u2d_draw_rectangle_srounded
/// u2d_draw_rectangle_srounded(x,y,w,h,r1,r2,r3,r4,outline)
var type; if(argument8 == true) type = pr_linestrip; else type = pr_trianglefan;
draw_primitive_begin(type)
  for(var i=0;i<=360;i+=5){
    var xx,yy,r;
    switch (i div 90)
    {
        case 1: r=argument5; break;
        case 2: r=argument6; break;
        case 3: r=argument7; break;
        case 0: default: r=argument4; break;
    }
     
    if(i<=180 || i==360) yy = argument1 + r; else yy = argument1 + argument3 - r;
    if(i>=90 && i<270) xx = argument0 + r; else xx = argument0 + argument2 - r;
    draw_vertex(xx+lengthdir_x(r,i),yy+lengthdir_y(r,i))
  }
draw_primitive_end();
#define u2d_draw_rectangle_srounded_color
/// u2d_draw_rectangle_srounded_color(x,y,w,h,c1,c2,c3,c4,r1,r2,r3,r4,s,outline)
var type; if(argument13) type = pr_linestrip; else type = pr_trianglefan;
if(argument9>90)argument9=90;
draw_primitive_begin(type)
  var xx=0, yy=0,xo,yo,c=0,d=0,r=0;
  if(!argument13)draw_vertex_colour(argument0+argument2/2,argument1+argument3/2,u2d_color_get_mix(argument4,argument5,argument6,argument7),1);
  for(part=0;part<=4;part++){
    xo=xx; yo=yy;
    switch (part)
    {
        case 1: d=90; r=argument9; xx=argument0+r; yy=argument1+r; c=argument5; break;
        case 2: d=180; r=argument10; xx=argument0+r; yy=argument1+argument3-r; c=argument6; break;
        case 3: d=270; r=argument11; xx=argument0+argument2-r; yy=argument1+argument3-r; c=argument7; break;
        default: d=0; r=argument8; xx=argument0+argument2-r; yy=argument1+r; c=argument4; break;
    }
    
    if(part!=4)for(var deg=0;deg<=90;deg+=argument12){
      if(r>0)draw_vertex_colour(xx + lengthdir_x(r,deg+d),yy + lengthdir_y(r,deg+d),c,1);
      else draw_vertex_colour(xx,yy,c,1);
    }else draw_vertex_colour(xx + r,yy,c,1);
  }
draw_primitive_end();
#define u2d_draw_sprite_tiled_h
/// u2d_draw_sprite_tiled_h(sprite, img, x, y, xscale, yscale, rot, color, alpha, sep);
var sprite_start, sprite_end, sprite_current, sprite_step;
       
sprite_step  = sprite_get_width(argument0)+argument9;
sprite_start = view_xview+((argument2-view_xview) mod sprite_step)-sprite_step;
sprite_end   = view_xview+view_wview+sprite_step;

for (sprite_current = sprite_start; sprite_current <= sprite_end; sprite_current += sprite_step)
draw_sprite_ext(argument0, argument1, sprite_current, argument3, argument4, argument5, argument6, argument7, argument8);
#define u2d_draw_sprite_tiled_v
/// u2d_draw_sprite_tiled_v(sprite, img, x, y, xscale, yscale, rot, color, alpha, sep);
var sprite_start, sprite_end, sprite_current, sprite_step;
       
sprite_step  = sprite_get_height(argument0)+argument9;
sprite_start = view_yview+((argument3-view_yview) mod sprite_step)-sprite_step;
sprite_end   = view_yview+view_hview+sprite_step;

for (sprite_current = sprite_start; sprite_current <= sprite_end; sprite_current += sprite_step)
draw_sprite_ext(argument0, argument1, argument2, sprite_current, argument4, argument5, argument6, argument7, argument8);
#define u2d_draw_text_outline
/// u2d_draw_text_outline(x,y,text,lcol);
draw_text_color( argument0, argument1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0+1, argument1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0, argument1+1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0+1, argument1+1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0+1, argument1-1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0-1, argument1+1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0-1, argument1-1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0-1, argument1, argument2, argument3, argument3, argument3, argument3, 1)
draw_text_color( argument0, argument1-1, argument2, argument3, argument3, argument3, argument3, 1)
#define u2d_draw_text_shadow
///u2d_draw_text_shadow(x,y,text,color,alpha,len,dir)
draw_text_colour(argument0+lengthdir_x(argument5,argument6),argument1+lengthdir_y(argument5,argument6),argument2,argument3,argument3,argument3,argument3,argument4);
