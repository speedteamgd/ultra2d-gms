#define u2d_cam_init
// u2d_cam_init()

cam_init = false;

cam_mode = 0; // �����
cam_view = 0;

// ���� ������
cam_obj = noone;
cam_tx = x;
cam_ty = y;

// ������ ������
cam_width = 640;
cam_height = 480;

// ���������� ������
cam_blocked = false;
cam_block_obj = noone;
cam_block_x_min = 0;
cam_block_x_max = room_width;
cam_block_y_min = 0;
cam_block_y_max = room_height;

// ����������� ���� ������
cam_x = x;
cam_y = y;
cam_dir_x = 0;
cam_dir_y = 0;
cam_offsetx = 100;
cam_offsety = 100;

// �������� ������
cam_dis_x = 0;
cam_dis_y = 0;
cam_smooth = 0;

// �������
shake = 0;
shake_power = 10;
shake_x = 0;
shake_y = 0;

blur_surf = surface_create(view_wview,view_hview);
blur_temp = surface_create(view_wview,view_hview);
blur_int = 2;
blur = 0;
#define u2d_cam_update
// Get target pos

var view_part_w = view_wview[cam_view]/2;
var view_part_h = view_hview[cam_view]/2;

switch (cam_mode)
{
    case 1:
      if instance_exists(cam_obj){
        cam_x = cam_obj.x;
        cam_y = cam_obj.y;
      }
    break;
    case 2:
        cam_x = cam_tx;
        cam_y = cam_ty;
    break;
}

if(cam_dir_x!=0)cam_x+=cam_offsetx*sign(cam_dir_x);
if(cam_dir_y!=0)cam_y+=cam_offsety*sign(cam_dir_y);

// Moving & block
if !cam_blocked{
    if(cam_smooth<2){ x = cam_x; y = cam_y; }
    else {
      cam_dis_x = cam_x-x;
      cam_dis_y = cam_y-y;
      x+=cam_dis_x/cam_smooth; if(abs(cam_dis_x) < 1)x=cam_x;
      y+=cam_dis_y/cam_smooth; if(abs(cam_dis_y) < 1)y=cam_y;
    }
  
  if(cam_block_obj!=noone){
      var cb_obj = instance_nearest(x,y,cam_block_obj);
      if(cb_obj.x > x && x >= cb_obj.x - view_part_w)x=cb_obj.x-view_part_w;
      if(cb_obj.x < x && x <= cb_obj.x + view_part_w)x=cb_obj.x+view_part_w;
      if(cb_obj.y > y && y >= cb_obj.y - view_part_h)y=cb_obj.y-view_part_h;
      if(cb_obj.y < y && y <= cb_obj.y + view_part_h)y=cb_obj.y+view_part_h;
  }
  
  if(x<cam_block_x_min+view_part_w)x=cam_block_x_min+view_part_w;
  if(x>cam_block_x_max-view_part_w)x=cam_block_x_max-view_part_w;
  if(y<cam_block_y_min+view_part_h)y=cam_block_y_min+view_part_h;
  if(y>cam_block_y_max-view_part_h)y=cam_block_y_max-view_part_h;
}

if(shake-->0){
  shake_x = irandom_range(-shake_power,shake_power);
  shake_y = irandom_range(-shake_power,shake_power);
} else {shake_x=0; shake_y=0;}

if(blur>0){
surface_copy(blur_surf,0,0,application_surface);
repeat(blur) {
     surface_set_target(blur_temp);
     draw_surface_ext(blur_surf,0,0,1/blur_int,1/blur_int,0,-1,1);
     surface_reset_target();
     surface_set_target(blur_surf);
     draw_surface_ext(blur_temp,0,0,blur_int,blur_int,0,-1,1);
     surface_reset_target();
}
}

view_xview[cam_view] = x-view_part_w+shake_x;
view_yview[cam_view] = y-view_part_h+shake_y;
#define u2d_cam_border_collision_right
// u2d_cam_border_collision(cam,x,y);
var xx = view_xview[view_current] -argument0;
var yy = view_yview[view_current] -argument1;
var ww = view_wview[view_current];
var hh = view_hview[view_current];
return collision_rectangle(xx+ww,yy,xx+ww-32,yy+hh,object_index,true,false);
#define u2d_cam_border_collision_left
// u2d_cam_border_collision(cam,x,y);
var xx = view_xview[view_current] -argument0;
var yy = view_yview[view_current] -argument1;
var ww = view_wview[view_current];
var hh = view_hview[view_current];
return collision_rectangle(xx,yy,xx-32,yy+hh,object_index,true,false);
#define u2d_cam_border_collision_bottom
// u2d_cam_border_collision(cam,x,y);
var xx = view_xview[view_current] -argument0;
var yy = view_yview[view_current] -argument1;
var ww = view_wview[view_current];
var hh = view_hview[view_current];
return collision_rectangle(xx,yy+hh,xx+ww,yy+hh+32,object_index,true,false);
#define u2d_cam_border_collision_top
// u2d_cam_border_collision(x,y);
var xx = view_xview[view_current] -argument0;
var yy = view_yview[view_current] -argument1;
var ww = view_wview[view_current];
var hh = view_hview[view_current];
return collision_rectangle(xx,yy,xx+ww,yy-32,object_index,true,false);
#define u2d_cam_border_collision_horz
// u2d_cam_border_collision_horz(x,y)
return u2d_cam_border_collision_left(argument0,argument1) || u2d_cam_border_collision_right(argument0,argument1)
#define u2d_cam_border_collision_vert
// u2d_cam_border_collision_horz(x,y)
return u2d_cam_border_collision_top(argument0,argument1) || u2d_cam_border_collision_bottom(argument0,argument1)
